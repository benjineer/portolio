
// Globals
var _main;
var _slider;
var _currentNavItem;
var _sliderResetTimer;
var _cursor;

var _screenBackgroundColour;
var _screenTextColour;

var _caliperSpeed = 300;
var _caliperResetSpeed = 200;
var _sliderOffset = -50;
var _cursorSpeed = 100;

// Gets nav anchor by URL
function getNavItem(href) {
   return $('.calipers .shaft a[href="' + href + '"]');
}

// Sets caliper position to nav anchor
function setCaliperPosition(anchor, callback, noAnimation) {
   var newPosition = anchor.offset().left - _main.offset().left + _sliderOffset;
   _slider.stop();

   // Immediate
   if (noAnimation) {
      _slider.css('left', newPosition + 'px');

      if (callback) {
         callback();
      }
   }

   // Animated
   else {
      _slider.animate({ left: newPosition + 'px' }, _caliperSpeed, function () {
         if (callback) {
            callback();
         }
      });
   }
}

// Page load
$(function () {

   // Set controls
   _main = $('.main');
   _slider = $('.calipers .slider');
   _cursor = $('.screen .cursor');

   // Get current relative URL and nav item
   var pageNameIndex = window.location.pathname.lastIndexOf('/') + 1;
   var currentPageName = window.location.pathname.substr(pageNameIndex);
   _currentNavItem = getNavItem(currentPageName);

   // Make current nav item active and set caliper pos to it
   _currentNavItem.addClass('active');
   setCaliperPosition(_currentNavItem, null, true);
   
   // Handle keyboard element key click
   $('.key')
   .on('mousedown', function (event) {
      $(this).addClass('down');
   })
   .on('mouseup', function (event) {
      var key = $(this);
      key.removeClass('down');

      var url = key.data('url');
      var anchor = getNavItem(url);

      setCaliperPosition(anchor, function () {
         location.href = url;
      });
   });

   // Pop key back up if the mouse is dragged off the key before keyup
   $('html, body').on('mouseup', function () {
      $('.key').removeClass('down');
   });

   // Animate calipers on hover
   $('.calipers a')
   .on('mouseover', function () {
      if (_sliderResetTimer) {
         clearTimeout(_sliderResetTimer);
      }

      setCaliperPosition($(this));
   })
   .on('mouseout', function () {
      _sliderResetTimer = setTimeout(function () {
         setCaliperPosition(_currentNavItem);
      }, _caliperResetSpeed);
   });
});
